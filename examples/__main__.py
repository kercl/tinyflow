import sys
import subprocess
import base64

from http.server import HTTPServer, SimpleHTTPRequestHandler
from mimetypes import guess_type
from pathlib import Path

favicon = b'iVBORw0KGgoAAAANSUhEUgAA' \
          b'AAEAAAABCAQAAAC1HAwCAAAA' \
          b'C0lEQVR42mNkYAAAAAYAAjCB' \
          b'0C8AAAAASUVORK5CYII='


class ExampleServer(SimpleHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/':
            self.path = sys.argv[1]

        if self.path == '/favicon.ico':
            self.send_response(200)
            self.send_header('Content-type', "image/png")
            self.end_headers()

            self.wfile.write(base64.b64decode(favicon))
            return

        return SimpleHTTPRequestHandler.do_GET(self)


if __name__ == "__main__":
    webServer = HTTPServer(("localhost", 8000), ExampleServer)

    url = "http://localhost:8000"
    if sys.platform=='win32':
        subprocess.Popen(['start', url], shell= True)
    elif sys.platform=='darwin':
        subprocess.Popen(['open', url])
    else:
        try:
            subprocess.Popen(['xdg-open', url])
        except OSError:
            print("Cannot launch xdg-open.")

    print("Serving at {}".format(url))

    webServer.serve_forever()
    webServer.server_close()
