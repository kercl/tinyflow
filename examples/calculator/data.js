var graph_data = {
    "version": "0.0.1",
    "nodes": {
        "1": {
            "id": 1,
            "type": "NumberNode",
            "properties": {
                "x": 355.83333587646484,
                "y": 195
            }
        },
        "2": {
            "id": 2,
            "type": "NumberNode",
            "properties": {
                "x": 407.83333587646484,
                "y": 385
            }
        },
        "3": {
            "id": 3,
            "type": "AddNode",
            "properties": {
                "x": 707.1000061035156,
                "y": 264
            }
        },
        "4": {
            "id": 4,
            "type": "ResultNode",
            "properties": {
                "x": 1010,
                "y": 312.5
            }
        }
    },
    "connections": {
        "0": {
            "start": {
                "id": 1,
                "socket": "val_out"
            },
            "end": {
                "id": 3,
                "socket": "a"
            }
        },
        "1": {
            "start": {
                "id": 2,
                "socket": "val_out"
            },
            "end": {
                "id": 3,
                "socket": "b"
            }
        },
        "2": {
            "start": {
                "id": 3,
                "socket": "c"
            },
            "end": {
                "id": 4,
                "socket": "val_in"
            }
        }
    }
};