import {Node, Input, Output} from "../../src/index.js";
import {NumberInput} from "./inputs.js"

class MathNode extends Node {
    constructor() {
        super();

        this.addClasses("node");
    }
}

class BinaryOperator extends MathNode {
    constructor(op_symbol) {
        super();
        this.addClasses("node-BinaryOperator");

        this.title = document.createElement("span");
        this.title.textContent = op_symbol;

        Object.assign(this.title.style, {
            width: "100%",
            textAlign: "center",
            fontSize: "2.25em"
        });

        this.setInput("a", new Input("number", {
            elementBody: "a"
        }));
        this.setInput("b", new Input("number", {
            elementBody: "b"
        }));
        this.setOutput("c", new Output("number", {
            elementBody: "="
        }));
    }
}

export class AddNode extends BinaryOperator {
    constructor() {
        super("+");
    }
    evaluate(inputs) {
        return { c: inputs.a + inputs.b };
    }
}
AddNode.display_name = "Addition";

export class SubNode extends BinaryOperator {
    constructor() {
        super("-");
    }
    evaluate(inputs) {
        return { c: inputs.a - inputs.b };
    }
}
SubNode.display_name = "Subtraction";

export class TimesNode extends BinaryOperator {
    constructor() {
        super("×");
    }
    evaluate(inputs) {
        return { c: inputs.a * inputs.b };
    }
}
TimesNode.display_name = "Multiplication";

export class DivideNode extends BinaryOperator {
    constructor() {
        super("÷");
    }
    evaluate(inputs) {
        return { c: inputs.a / inputs.b };
    }
}
DivideNode.display_name = "Division";

export class NumberNode extends MathNode {
    constructor() {
        super();
        this.title = "Number";

        this.setInput("val_in", new NumberInput("number"));
        this.setOutput("val_out", new Output("number"));
    }

    evaluate(inputs) {
        return {
            val_out: inputs.val_in
        };
    }
}
NumberNode.display_name = "Number Input";

export class ResultNode extends MathNode {
    constructor() {
        super();
        this.title = "Result";
        this.addClasses("node-ResultNode");

        this.setInput("val_in", new Input("number", {
            name: ""
        }));
    }

    evaluate(inputs) {
        var value = Math.round(inputs.val_in * 1000) / 1000;
        this.body.textContent = `= ${value}`;
        this.body.style.padding = "0.6em";
    }
}
ResultNode.display_name = "Result Display";
