import {Input} from "../../src/index.js";

export class NumberInput extends Input {
    constructor() {
        var properties = {
            hasSocket: false
        };

        var el = document.createElement("input");
        properties.elementBody = el;

        super("number", properties);

        el.type = "input";
        el.value = "0";
        this.value = 0;
        el.style.width = "7em";

        el.onkeyup = function() {
            this.value = parseFloat(el.value);
            this.update();
        }.bind(this);
    }

    getDefaultValue() {
        return this.value;
    }
}
