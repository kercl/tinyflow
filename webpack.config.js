const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
    mode: 'development',
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({terserOptions: {
                                mangle: true,
                                compress: true,
                            }})
        ],
    },
};
