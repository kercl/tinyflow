const __tinyflow_version = "0.0.1";

class InteractiveElement {
    constructor() {
        this.callbacks = {};
    }

    drag(event, dom_target, editor, callback_id) {
        this.container.focus();

        this.drag_x = event.clientX;
        this.drag_y = event.clientY;

        var on_pointer_move = function(event) {
            event = event || window.event;
            event.preventDefault();
    
            var x = (this.drag_x - event.clientX) / editor.properties.scale;
            var y = (this.drag_y - event.clientY) / editor.properties.scale;
            this.drag_x = event.clientX;
            this.drag_y = event.clientY;
    
            this.evokeCallbacks(callback_id, {
                x: dom_target.offsetLeft - x,
                y: dom_target.offsetTop - y
            });
 
            dom_target.style.left = (dom_target.offsetLeft - x) + "px";
            dom_target.style.top = (dom_target.offsetTop - y) + "px";
        }.bind(this);
    
        var on_pointer_up = function(event) {
            document.removeEventListener("pointerup", on_pointer_up);
            document.removeEventListener("pointermove", on_pointer_move);
        }.bind(this);

        document.addEventListener("pointerup", on_pointer_up);
        document.addEventListener("pointermove", on_pointer_move);
    }

    addEventListener(callback_id, callback) {
        if(this.callbacks.hasOwnProperty(callback_id) == false) {
            this.callbacks[callback_id] = [];
        }

        this.callbacks[callback_id].push(callback);
    }

    evokeCallbacks(callback_id, ...args) {
        if(this.callbacks.hasOwnProperty(callback_id)) {
            this.callbacks[callback_id].forEach(
                callback => callback(this, ...args)
            );
        }
    }
}

export class Editor extends InteractiveElement {
    constructor(container_id, properties = {}) {
        super();

        this.properties = {
            zoom_speed: 0.07,
            scale: 1
        };
        Object.assign(this.properties, properties);

        this.node_classes = {};
        this.input_classes = {};
        this.output_classes = {};

        this.nodes = {};
        this.next_node_id = 0;
        this.connections = {};
        this.next_connection_id = 0;

        this.callbacks = {};

        this.container = document.getElementById(container_id);
        this.container.style.zIndex = 0;
        this.container.setAttribute("tabindex", "0");

        this.draw_area = document.createElement("div");
        this.container.appendChild(this.draw_area);
        
        Object.assign(this.draw_area.style, {
            position: "absolute",
            left: "0px",
            top: "0px",
            transformOrigin: "0px 0px"
        });

        var on_pointer_down = function(ev) {
            ev = ev || window.event;
            ev.preventDefault();
            ev.stopPropagation();

            this.drag(
                ev,
                this.draw_area,
                {properties: {scale: 1}},
                "editorDrag");
        }.bind(this);
        this.container.addEventListener("pointerdown", on_pointer_down);
        this.container.addEventListener("drop", function(ev) {
            this.evokeCallbacks("onDrop", ev);
        }.bind(this));
        this.container.addEventListener("dragover", function(event) {
            event.preventDefault();
        });
        this.container.addEventListener("wheel", this._zoom.bind(this));
    }

    _zoom(event) {
        event = event || window.event;
        event.preventDefault();
        
        var factor = Math.pow(
            1 + Math.abs(this.properties.zoom_speed),
            -Math.sign(event.deltaY)
        );

        var draw_area_x = parseFloat(this.draw_area.style.left),
            draw_area_y = parseFloat(this.draw_area.style.top);

        var editor_box = this.container.getBoundingClientRect();
        var rel_cx = event.clientX - editor_box.x,
            rel_cy = event.clientY - editor_box.y;

        var px = draw_area_x * factor + rel_cx * (1 - factor),
            py = draw_area_y * factor + rel_cy * (1 - factor);

        this.draw_area.style.left = `${px}px`;
        this.draw_area.style.top = `${py}px`;

        this.evokeCallbacks("editorZoom",
            this.properties.scale
        );

        this.properties.scale *= factor;
        this.draw_area.style.transform = `scale(${this.properties.scale})`;
    }

    serialize() {
        var res = {
            version: __tinyflow_version,
            nodes: {},
            connections: {}
        };

        Object.keys(this.nodes).forEach(
            k => res.nodes[k] = this.nodes[k].serialize());

        Object.keys(this.connections).forEach(
            k => res.connections[k] = this.connections[k].serialize());

        return JSON.stringify(res);
    }

    deserialize(data) {
        var max_id = 0;

        for(let id of Object.keys(data.nodes)) {
            if (id > max_id) {
                max_id = id;
            }

            this.next_node_id = id;
            var node = this.spawnNode(data.nodes[id].type)
            node.setPosition({
                x: data.nodes[id].properties.x,
                y: data.nodes[id].properties.y
            });
        }

        for(let id of Object.keys(data.connections)) {
            this.connect(
                data.connections[id].start.id,
                data.connections[id].start.socket,
                data.connections[id].end.id,
                data.connections[id].end.socket
            );
        }
    }

    append(object) {
        this.draw_area.appendChild(object);
    }

    remove(object) {
        this.draw_area.removeChild(object);
    }

    registerNodeTypes(...node_classes) {
        node_classes.forEach(function(node_class) {
            this.node_classes[node_class.name] = node_class;
        }.bind(this));
    }

    registerInputTypes(...input_classes) {
        input_classes.forEach(function(input_class) {
            this.input_classes[input_class.name] = input_class;
        }.bind(this));
    }

    registerOutputTypes(...output_classes) {
        output_classes.forEach(function(output_class) {
            this.output_classes[output_class.name] = output_class;
        }.bind(this));
    }

    spawnNode(node_name, ...args) {
        var node = new this.node_classes[node_name](...args);
        this.addNode(node);

        return node;
    }

    addNode(node) {
        node.register(this);
        node.id = this.next_node_id;
        this.next_node_id += 1;
        this.nodes[node.id] = node;

        node.draw();

        return node.id;
    }

    removeNode(node) {
        if(typeof node == "number") {
            var node_id = node;
        } else {
            var node_id = node.id;
        }

        node.evokeCallbacks("removeNode");
        node.onRemove();

        for(var key in this.nodes[node_id].inputs) {
            var inputs = this.nodes[node_id].inputs[key];

            while(inputs.connections.length > 0) {
                this.removeConnection(inputs.connections[0]);
            }
        }

        var update_nodes = new Set();

        for(var key in this.nodes[node_id].outputs) {
            var outputs = this.nodes[node_id].outputs[key];

            while(outputs.connections.length > 0) {
                update_nodes.add(outputs.connections[0].end_node);
                this.removeConnection(outputs.connections[0]);
            }
        }

        this.remove(this.nodes[node_id].container);
        delete this.nodes[node_id];

        for (let node of update_nodes) {
            node.update();
        }
    }

    addConnection(connection) {
        connection.register(this);
        connection.id = this.next_connection_id;
        this.next_connection_id += 1;
        this.connections[connection.id] = connection;
        connection.draw();
    }

    removeConnection(connection) {
        var outputs = connection.start_node.outputs[connection.start_socket_id],
            inputs = connection.end_node.inputs[connection.end_socket_id];

        var idx = outputs.connections.indexOf(connection);
        if (idx > -1) {
            outputs.connections.splice(idx, 1);
        }

        idx = inputs.connections.indexOf(connection);
        if (idx > -1) {
            inputs.connections.splice(idx, 1);
        }

        this.remove(connection.connection_path.getPath());
    }

    connect(start_node_id, start_socket_id, end_node_id, end_socket_id) {
        var conn = new Connection(
            this.nodes[start_node_id],
            start_socket_id,
            this.nodes[end_node_id],
            end_socket_id,
            {
                type: this.nodes[start_node_id].outputs[start_socket_id].type
            }
        );

        this.addConnection(conn);
        
        return conn;
    }

    transformScreenToDrawArea(p) {
        var draw_area_rect = this.draw_area.getBoundingClientRect();

        return {
            x: (p.x - draw_area_rect.x) / this.properties.scale,
            y: (p.y - draw_area_rect.y) / this.properties.scale
        }
    }

    run() {
        var starting_nodes = [];

        for(var node_id in this.nodes) {
            var node_is_end = true;

            this.nodes[node_id].visit_counter = 0;

            for(var key in this.nodes[node_id].outputs) {
                if(this.nodes[node_id].outputs[key].connections.length > 0) {
                    node_is_end = false;
                }
            }
            
            if(node_is_end) {
                starting_nodes.push(this.nodes[node_id]);
            }
        }

        starting_nodes.forEach(node => node._evaluate());
    }
}

export class Node extends InteractiveElement {
    constructor(properties) {
        super();

        this.properties = {
            x: 0,
            y: 0
        }
        Object.assign(this.properties, properties);

        this.container = undefined;
        this.classList = [];

        this.callbacks = {};
        this.inputs = {};
        this.outputs = {};

        this.visit_counter = 0;

        this.body = document.createElement("div");
        this.title = null;

        this.addEventListener("nodeDrag", this._updatePositionProperty);
    }

    serialize() {
        return {
            id: this.id,
            type: this.constructor.name,
            properties: this.properties
        };
    }

    addClasses(...classes) {
        this.classList.push(...classes);
    }

    setPosition(p) {
        var b_box = this.container.getBoundingClientRect();

        this.properties.x = p.x;
        this.properties.y = p.y;

        Object.assign(this.container.style, {
            left: `${p.x - b_box.width / 2}px`,
            top: `${p.y - b_box.height / 2}px`
        })
    }

    register(editor) {
        this.editor = editor;
    }

    evaluate(inputs) { return {}; }

    update() {
        this.visit_counter = 0;
        this._evaluate();

        for(var key in this.outputs) {
            this.outputs[key].connections.forEach(function(connection) {
                connection.end_node.update();
            });
        }
    }

    draw() {
        this.container = document.createElement("div");
        this.classList.forEach((cls => this.container.classList.add(cls)).bind(this))

        this.container.addEventListener("keydown", function(ev) {
            if(ev.key == "Delete") {
                this.editor.removeNode(this);
            }
        }.bind(this));

        this.container.addEventListener("pointerdown", function(ev) {
            ev = ev || window.event;
            ev.preventDefault();
            ev.stopPropagation();

            this.drag(
                ev,
                this.container,
                this.editor,
                "nodeDrag");
        }.bind(this));

        this.container.addEventListener("click", function(ev) {
            this.container.focus();
        }.bind(this));

        this.editor.append(this.container);

        if(this.title) { 
            if(typeof this.title === "string") {
                var title_element = document.createElement("span");
                title_element.classList.add("title");
                title_element.textContent = this.title;
                this.container.appendChild(title_element);
            } else {
                this.title.classList.add("title");
                this.container.appendChild(this.title);
            }
        }

        var content = document.createElement("div");
        Object.assign(content.style, {
            display: "flex",
            flexDirection: "row",
            flexGrow: "1",
            width: "100%"
        });
        this.container.appendChild(content);

        this.inputs_div = document.createElement("div");
        Object.assign(this.inputs_div.style, {
            marginLeft: "0px"
        })
        content.appendChild(this.inputs_div);

        content.appendChild(this.body);
        this.body.style.width = "100%";

        this.outputs_div = document.createElement("div");
        Object.assign(this.outputs_div.style, {
            marginRight: "0px",
            float: "right"
        })
        content.appendChild(this.outputs_div);

        for(var key in this.inputs) {
            this.inputs_div.appendChild(this.inputs[key].draw())
        }

        for(var key in this.outputs) {
            this.outputs_div.appendChild(this.outputs[key].draw())
        }

        Object.assign(this.container.style, {
            overflow: "hidden",
            position: "absolute",
            zIndex: 2
        });
        this.setPosition(this.properties);

        this.container.setAttribute("tabindex", "0");
    }

    onRemove() {}

    setInput(key, object) {
        if(object.properties.isOutput) {
            throw "Object needs to be an input."
        }

        if(this.outputs.hasOwnProperty(key)) {
            throw "Key is already registered as output socket.";
        }

        object.node = this;
        object.key = key;
        this.inputs[key] = object;
    }

    setOutput(key, object) {
        if(!object.properties.isOutput) {
            throw "Object needs to be an output."
        }

        if(this.inputs.hasOwnProperty(key)) {
            throw "Key is already registered as input socket.";
        }

        object.node = this;
        object.key = key;
        this.outputs[key] = object;
    }

    _updatePositionProperty(self, p) {
        var b_box = self.container.getBoundingClientRect();

        self.properties.x = p.x + b_box.width / 2;
        self.properties.y = p.y + b_box.height / 2;
    }

    _evaluate() {
        if(this.visit_counter > 0)
            return this._current_result;

        var inputs = {};
        for(var key in this.inputs) {
            inputs[key] = this.inputs[key].getValue();
        }

        this.visit_counter += 1;
        this._current_result = this.evaluate(inputs);

        if(Object.keys(this.outputs).length > 0) {
            if(typeof this._current_result != "object") {
                throw "evaluate() must return an object containing a key for each output socket.";
            }
            for(var key in this.outputs) {
                if(!this._current_result.hasOwnProperty(key)) {
                    throw "evaluate() must return an object containing a key for each output socket.";
                }

                this.outputs[key].onUpdate(this._current_result[key]);
            }
        } else {
            this._current_result = {};
        }

        return this._current_result;
    }
}

class ConnectionPath {

    constructor(p1, p2, properties) {
        this.x1 = p1.x;
        this.y1 = p1.y;

        this.x2 = p2.x;
        this.y2 = p2.y;

        this.properties = {
            bendLength: 100,
            stroke: '#aaaaaa',
            className: 'connection',
            type: null
        };
        Object.assign(this.properties, properties);
        this.canvas = undefined;
    }

    _updatePathParameters() {
        Object.assign(this.canvas.style, {
            left: `${this.x1}px`,
            top: `${this.y1}px`
        });

        var dx = this.x2 - this.x1;
        var dy = this.y2 - this.y1;

        const min_x = 20;
        var l = Math.max(Math.abs(dx) * 1 / 3, 2 * min_x);

        if(dx**2 + dy**2 > min_x**2) {
            this.path.setAttribute('d',
                `M0,0 ` +
                `L${min_x},0 ` +
                `C${min_x + l},0 ${dx - l - min_x},${dy} ${dx - min_x},${dy} ` +
                `L${dx},${dy}`
            );
        } else {
            this.path.setAttribute('d',
                `M0,0 ` +
                `L${dx},0`
            );
        }
    }

    getPath() {
        if(this.canvas) {
            return this.canvas;
        }

        this.canvas = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        Object.assign(this.canvas.style, {
            position: "absolute",
            width: "1px",
            height: "1px",
            zIndex: 1,
        });
        this.canvas.style.overflow = "visible";

        this.path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        this.path.setAttribute("fill", "transparent");
        this.path.setAttribute(this.properties.type, "");
        this.path.classList.add(this.properties.className);

        this._updatePathParameters();

        this.canvas.appendChild(this.path);

        return this.canvas;
    }

    setStart(p) {
        this.x1 = p.x;
        this.y1 = p.y;

        this._updatePathParameters();
    }

    setEnd(p) {
        this.x2 = p.x;
        this.y2 = p.y;

        this._updatePathParameters();
    }
}

export class Connection {
    constructor(start_node, start_socket_id, end_node, end_socket_id, properties = {}) {
        if(start_node.outputs[start_socket_id].type != end_node.inputs[end_socket_id].type) {
            throw "datatype mismatch between start and end socket."
        }   

        if(start_node == end_node) {
            throw "start node and end node cannot be the same.";
        }

        if(end_node.inputs[end_socket_id].connections.length > 0) {
            throw "Number of input connections cannot exceed mor than one.";
        }

        start_node.outputs[start_socket_id].connections.push(this);
        end_node.inputs[end_socket_id].connections.push(this);

        this.start_node = start_node;
        this.start_socket_id = start_socket_id;
        this.end_node = end_node;
        this.end_socket_id = end_socket_id;

        this.start_node.addEventListener("nodeDrag", this._updateEndpoints.bind(this));
        this.end_node.addEventListener("nodeDrag", this._updateEndpoints.bind(this));

        this.properties = properties;
    }

    serialize() {
        return {
            start: {
                id: this.start_node.id,
                socket: this.start_socket_id
            },
            end: {
                id: this.end_node.id,
                socket: this.end_socket_id
            }
        };
    }

    register(editor) {
        this.editor = editor;
    }

    draw() {
        this.connection_path = new ConnectionPath(
            this.start_node.outputs[this.start_socket_id].getPosition(),
            this.end_node.inputs[this.end_socket_id].getPosition(),
            this.properties
        );

        this.editor.append(this.connection_path.getPath());
    }

    _updateEndpoints(x, y) {
        this.connection_path.setStart(
            this.start_node.outputs[this.start_socket_id].getPosition()
        );
        this.connection_path.setEnd(
            this.end_node.inputs[this.end_socket_id].getPosition()
        );
    }
}

class IOElement {
    constructor(type, properties) {
        this.type = type;
        this.connections = [];
        this.properties = {
            elementBody: "",
            padding: "10px",
            socketMargin: "10px",
            isOutput: true,
            hasSocket: true
        };
        Object.assign(this.properties, properties);
    }

    getPosition() {
        var rect = this.socket.getBoundingClientRect();
        return this.node.editor.transformScreenToDrawArea({
            x: rect.x + rect.width / 2,
            y: rect.y + rect.height / 2
        });
    }

    removeDragPath() {
        if(this.node.editor._drag_path) {
            this.node.editor.remove(this.node.editor._drag_path.getPath());
            this.node.editor._drag_path = undefined;
        }
    }

    _drawConnection(event) {
        event = event || window.event;
        event.preventDefault();
        event.stopPropagation();

        this.removeDragPath();

        if(!this.properties.isOutput) {
            if(this.node.inputs[this.key].connections.length == 1) {
                var connection = this.node.inputs[this.key].connections[0];
                connection.start_node.outputs[connection.start_socket_id]._drawConnection(event);
                this.node.editor.removeConnection(connection);
                return;
            }
            
        }

        var p1 = this.getPosition(),
            p2 = this.node.editor.transformScreenToDrawArea({
            x: event.clientX,
            y: event.clientY
        });

        this.node.editor._drag_path = new ConnectionPath(p1, p2, {
            type: this.type
        });
        this.node.editor._first_io_element = this;

        this.node.editor.append(this.node.editor._drag_path.getPath());

        var on_pointer_move = function(event) {
            event = event || window.event;
            event.preventDefault();
            event.stopPropagation();

            var p = this.node.editor.transformScreenToDrawArea({
                x: event.clientX,
                y: event.clientY
            });

            if(this.properties.isOutput) {
                this.node.editor._drag_path.setEnd(p);
            } else {
                this.node.editor._drag_path.setStart(p);
            }
        }.bind(this);

        var on_pointer_up = function() {
            document.removeEventListener("pointermove", on_pointer_move);
            document.removeEventListener("pointerup", on_pointer_up);
            
            this.removeDragPath();
        }.bind(this);

        document.addEventListener("pointermove", on_pointer_move);
        document.addEventListener("pointerup", on_pointer_up);
    }

    _createSocket() {
        this.socket = document.createElement("div");

        this.socket.onpointerdown = this._drawConnection.bind(this);
        this.socket.onpointerup = this._connectSockets.bind(this);

        this.socket.classList.add("socket");
        this.socket.classList.add(this.type);
        this.socket.setAttribute(this.type, "");
        Object.assign(this.socket.style, {
            position: "relative",
            left: 0,
            top: 0
        });
    }

    _connectSockets() {
        var first = this.node.editor._first_io_element;
        var second = this;

        if(second.properties.isOutput) {
            var tmp = first;
            first = second;
            second = tmp;
        }

        this.node.editor.connect(first.node.id, first.key, second.node.id, second.key);
        second.node.update();
    }

    draw() {
        var container = document.createElement("div");

        if(typeof this.properties.elementBody == "string") {
            var display_element = document.createElement("span")
            display_element.textContent = this.properties.elementBody;
        } else {
            var display_element = this.properties.elementBody;
            container.onpointerdown = function(ev) {
                ev.stopPropagation();
            }
            container.onmousedown = function(ev) {
                ev.stopPropagation();
            }
            container.onclick = function(ev) {
                ev.stopPropagation();
            }
            container.onkeydown = function(ev) {
                ev.stopPropagation();
            }
            container.onkeypress = function(ev) {
                ev.stopPropagation();
            }
            container.onkeyup = function(ev) {
                ev.stopPropagation();
            }
            container.onwheel = function(ev) {
                ev.stopPropagation();
            }
        }

        if(this.properties.isOutput) {
            var justify = "flex-end";
            Object.assign(display_element.style, {
                marginRight: this.properties.socketMargin
            });
            this._createSocket();
            container.appendChild(display_element);
            container.appendChild(this.socket);
        } else {
            var justify = "flex-start";
            Object.assign(display_element.style, {
                marginLeft: this.properties.socketMargin
            });
            if(this.properties.hasSocket) {
                this._createSocket();
                container.appendChild(this.socket);
            }
            container.appendChild(display_element);
        }

        Object.assign(container.style, {
            margin: this.properties.padding,
            boxSizing: "border-box",
            display: "flex",
            alignItems: "center",
            justifyContent: justify
        });

        return container;
    }
}

export class Input extends IOElement {
    constructor(type, properties = {}) {
        properties.isOutput = false;
        super(type, properties);
    }

    getValue() {
        if(this.connections.length == 0) {
            return this.getDefaultValue();
        }
        var prev_node_output = this.connections[0].start_node._evaluate();
        return prev_node_output[this.connections[0].start_socket_id]
    }

    getDefaultValue() {
        return null;
    }

    update() {
        this.node.update();
    }
}

export class Output extends IOElement {
    constructor(type, properties = {}) {
        properties.isOutput = true;
        super(type, properties);
    }

    onUpdate() {}
}
